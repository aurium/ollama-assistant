#!/usr/bin/env node

const NODE_VERSION = parseFloat(process.version.substring(1))
if (NODE_VERSION < 22.2) {
    console.error('Must use Node v22.2 or above.')
    process.exit(1)
}

// TODO:
// List Local Models
// Show Model Information

const { request } = require('node:http')
const { spawn } = require('node:child_process')
const { glob, readFile, writeFile } = require('node:fs/promises')

const args = process.argv
    .filter(a=>/^--[a-z]/.test(a))
    .map(a => a.split('='))
    .reduce((memo, [key, val])=> {
        key = key.substr(2)
        if (memo.hasOwnProperty(key)) {
            if (!Array.isArray(memo[key])) memo[key] = [memo[key]]
            memo[key].push(val??true)
        }
        else memo[key] = val??true
        return memo
    }, {})

const APP_ROOT = __dirname
const DATA_DIR = APP_ROOT + '/ollama-data'
const MODEL = args.model || 'llama3'
const PULL_LOCK = `${DATA_DIR}/last-${MODEL}-pull`
const LANG = (args.lang || process.env.LANG).split('.')[0]

if ( !module.parent && (Object.keys(args).length===0 || args.help) ) {
    console.log(`
    Will setup and run a question to a LLM on Ollama, with special support for programmers.

    Usage:
    $ code-assistant --<paramStr>=<val> --<paramBool> ... --<lastParam>=<lastVal>

    Example:
    $ code-assistant --docker-auto --model=codestral --query="O que é um escaravelho?"

    Parameters:
      --docker-up       Run ollama docker, if it is not running.
      --docker-down     Stops the ollama docker.
      --docker-auto     The same as using both --docker-up and --docker-down.
      --model=<name>    Set the LLM that will work as an code reviewer.
      --auto-pull       Pulls the model if last pull is old. Defalt is equal --docker-up.
      --force-pull      Pulls the model anyway.
      --lang=<name>     Set the language to answer. If unset, will use LANG env var.
      --ctx-file=<file> Adds a file to the assistant context. You can repeat or use globs.
      --query=<text>    Send a question to the LLM.
    `)
    process.exit(0)
}

if (args['docker-auto']) {
    args['docker-up'] = true
    args['docker-down'] = true
}

if (args['docker-up']) args['auto-pull'] ??= true
args['auto-pull'] = JSON.parse(String(args['auto-pull'] || false))

const queryContext = []

console.log('>> Debug args:', args)

const ctxFiles = module.exports.ctxFiles =
async function ctxFiles(files) {
    if (!Array.isArray(files)) files = [files]
    const cwdRE = new RegExp('^'+process.cwd())
    const loadingFiles = (await Promise.all(
        files.map(f => Array.fromAsync(glob(f, { withFileTypes: true })))
    ))
    .flat()
    .filter(f => f.isFile())
    .map(f => {
        // The non glob entries come with filename on path, glob ones don't.
        // So we nee to test the pathTail against the filename.
        const pathTail = f.path.substring( f.path.length - f.name.length )
        const fullPath = (pathTail === f.name) ? f.path : f.path+'/'+f.name
        const path = fullPath.replace(cwdRE, '.') // makes relative to CWD.
        return readFile(fullPath, 'utf8').then(content => ({ path, content }))
    })
    ;(await Promise.all(loadingFiles))
    .map(entry => {
        console.log('>> File added to context:', entry.path)
        queryContext.push({
            role: 'user',
            content: `file \`${entry.path}\` content:\n\`\`\`\n${entry.content}\n\`\`\``
        })
    })
}

const query = module.exports.query =
function query(text) {
    const systemI18n = {
      'pt_BR': 'Você é um brasileiro revisor de código e deve escrever em português.',
      'pt': 'Tu és um português revisor de código e deves escrever em português.',
      'es': 'Eres un revisor de códigos y debes escribir tu respuesta en español.',
      'en': 'You are a code reviewer and should write your answer in english.',
      '*': `You are a code reviewer and should write your answer in ${LANG}.`
    }
    const systemL10n = systemI18n[LANG] || systemI18n[LANG.split('_')[0]] || systemI18n['*']
    const data = {
        model: MODEL,
        messages: [
            ...queryContext,
            { role: 'system', content: systemL10n },
            { role: 'user', content: text }
        ]
    }
    console.log('CHAT:', data)
    const start = Date.now()
    let waiter = setInterval(()=> {
        const time = periodUntilNow(start)
        process.stdout.write(`>> Waiting for ${time}...\r`)
    }, 1000)
    return new Promise((resolve, reject)=> {
        const url = 'http://localhost:11434/api/chat'
        const req = request(url, { method: 'POST' }, res => {
            let parsedOut = ''
            res.on('data', chunk => {
                if (waiter) {
                    clearInterval(waiter)
                    waiter = null
                    process.stdout.write(' '.repeat(30)+'\r')
                }
                try {
                    parsedOut += parseResponseLine(chunk)
                } catch(err) {
                    reject(err)
                }
            })
            res.on('end', ()=> {
                const time = periodUntilNow(start)
                console.log(`>> Total time to answer: ${time}`)
                resolve(parsedOut)
            })
        })
        req.on('error', reject)
        req.write(JSON.stringify(data))
        req.end()
    })
}

function periodUntilNow(start) {
    const secs = Math.round((Date.now() - start) / 1000)
    return Math.floor(secs/60) +':'+ String(secs%60).padStart(2, 0)
}

function parseResponseLine(line) {
    const data = JSON.parse(line)
    if (data.error) throw Error(data.error)
    if (data.done) process.stdout.write('\n>> DONE\n')
    const chunk = data.message?.content || ''
    process.stdout.write(chunk)
    return chunk
}

async function autoPullModel() {
    const lastPull = new Date(await readFile(PULL_LOCK, 'utf8').catch(()=> 0))
    const twoDays = 60_000 * 60 * 24 * 2
    if ((Date.now() - lastPull) > twoDays) return pullModel()
}

async function pullModel() {
    const start = Date.now()
    await new Promise((resolve, reject)=> {
        const url = 'http://localhost:11434/api/pull'
        const req = request(url, { method: 'POST' }, res => {
            res.on('data', chunk => {
                try {
                    parsePullLine(chunk)
                } catch(err) {
                    reject(err)
                }
            })
            res.on('end', ()=> {
                const time = periodUntilNow(start)
                console.log(`>> Total time to pull: ${time}`)
                resolve(true)
            })
        })
        req.on('error', reject)
        req.write(JSON.stringify({ name: MODEL }))
        req.end()
    })
    return writeFile(PULL_LOCK, (new Date()).toJSON())
    
    /*
    curl http://localhost:11434/api/pull -d "{ \"name\": \"$MODEL\" }" |
    while read line; do
        err=$(       echo "$line" | jq .error --raw-output )
        status=$(    echo "$line" | jq .status --raw-output )
        total=$(     echo "$line" | jq .total//0 )
        completed=$( echo "$line" | jq .completed//0 )
        if test -n "$err" -a "$err" != "null"; then
            echo "# ERROR: $err"
            dye "$err"
        fi
        if test -n "$status" -a $total -gt 0; then
            pct=$( echo "100*$completed/$total" | bc -l | sed 's/^\./0./' )
            echo -en "\r>> $status -- $pct%                                     " >&2
            echo "# $status -- $(echo $pct | cut -d. -f1)%"
            echo $pct
        else
            echo ">> $line" >&2
        fi
    done |
    zenity --progress \
           --title="Pulling $MODEL Model" \
           --text="..." \
           --width=800 \
           --auto-close \
           --percentage=0
    */
}

function parsePullLine(line) {
    const data = JSON.parse(line)
    if (data.error) throw Error(data.error)
    const status = data.status || ''
    const total = data.total || 1
    const completed = data.completed || 0
    const pct = (100 * completed / total).toFixed(2)
    process.stdout.write(`>> ${status}  ${pct}%      \r`)
    if (pct === '100.00') process.stdout.write('\n>> DONE\n')
}

async function dockerUp() {
    // The webui is not working, so the docker compose is off.
    // docker compose -f code-assistant-compose.yml up
    const isRunning = await new Promise((resolve, reject)=> {
        // docker ps --filter name=ollama -q
        const ps = spawn('docker', ['ps', '--filter', 'name=ollama', '-q'])
        let stdout = '', stderr = ''
        ps.stdout.on('data', data => stdout += data.toString())
        ps.stderr.on('data', data => stderr += data.toString())
        ps.on('close', code => {
            if (code === 0) resolve(stdout.trim().length > 0) // Found ollama?
            else reject(Error('Docker ps FAIL! ' + stderr))
        })
    })
    if (!isRunning) {
        // docker run --rm --name ollama -v /ollama-data:/root/.ollama ollama/ollama
        spawn('docker', [
            'run',
            '--rm',
            '--name', 'ollama',
            '-p', '11434:11434',
            '-v', DATA_DIR+':/root/.ollama',
            'ollama/ollama'
        ], { stdio: 'inherit' })
        await new Promise(resolve => setTimeout(resolve, 1000))
    }
}

function dockerDown() {
    return new Promise((resolve, reject)=> {
        const ps = spawn('docker', ['stop', 'ollama'])
        let stderr = ''
        ps.stderr.on('data', data => stderr += data.toString())
        ps.on('close', code => {
            if (code === 0) resolve(true)
            else reject(Error('Docker stop FAIL! ' + stderr))
        })
    })
}

module.exports.init = async function init() {
    if (args['docker-up'])   await dockerUp()
    if (args['force-pull'])  await pullModel()
    if (args['auto-pull'])   await autoPullModel()
}

module.exports.finish = async function finish() {
    if (args['docker-down']) await dockerDown()
}

if (!module.parent) {
    (async ()=> {
        module.exports.init()
        if (args['ctx-file'])  await ctxFiles(args['ctx-file'])
        if (args['query'])     await query(args['query'])
        module.exports.finish()
    })()
    .catch(err => console.error('Assistant CLI fail:', err))
}

